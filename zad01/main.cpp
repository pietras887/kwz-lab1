#include "projekt.h"

int main()
{
	clock_t start, koniec;
	std::ofstream plik("output2.txt");
		std::string nazwa;
		for (int i = 1; i <= 1; i++){
			nazwa = "plik" + std::to_string(i) + ".txt";
			std::cout << "nazwa " << nazwa << std::endl;
			Projekt p(nazwa.c_str());
			std::cout << "Algorytm Belmana-Forda" << std::endl;
			start = clock();
			std::cout << "Czas projektu: " << p.czasProjektuFB() << std::endl;
			koniec = clock();
			long delta = (long)(koniec - start);
			plik << delta << ' ';
			std::cout << "t1: " << delta << std::endl;
			p.getParametryDzialania();
			auto sciezka = p.getSciezkaKrytyczna();
			std::cout << "Sciezka krytyczna" << std::endl;
			for (auto i : sciezka)
				std::cout << i << std::endl;
			start = clock();
			p.Sortowanie();
			p.Relaksacja();
			koniec = clock();
			delta = (long)(koniec - start);
			plik << delta << std::endl;
			std::cout << "t2: " << delta << std::endl;
			std::cout << "Relaksacja w porzadku topologicznym" << std::endl;
			p.getParametryDzialania();
	}
	system("pause");

}