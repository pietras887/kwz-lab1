#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <limits>
#include <queue>
#include <time.h>
#include <sstream>

class Projekt
{
	struct dzialanie
	{
		int earlyStart;
		int earlyFinish;
		int lateStart;
		int lateFinish;
	};
	struct polaczenia
	{
		int nr;
		int wchodzace;
		int wychodzace;
		polaczenia( ) {
			nr = 0;
			wchodzace = 0;
			wychodzace = 0;
		}
	};
	int lw, lk;
	int czas_projektu;
	int* czas_trwania;
	int** macierz;
	int czas;
	std::vector<int> sciezkaKrytyczna;
	dzialanie* daneDzialania;

	std::vector<polaczenia> Polaczenia;
	std::vector<int> posortowaneWierzcholki;

	void inicjuj();
	std::vector<int> czynnosciKoncowe();
	std::vector<int> wyznaczPoprzedniki(const int i);
	void wyznaczParametryDzialaniaLate(const int i);
	void wyznaczSciezkaKrytyczna(const int koniec, std::vector<int> tabPoprzednikow);
public:
	Projekt(std::string nazwa_pliku);
	~Projekt();
	bool wczytaj(std::string nazwa_pliku);
	int czasProjektuFB();
	std::vector<int> getSciezkaKrytyczna();
	void getParametryDzialania();
	void Sortowanie();
	void Relaksacja();
};