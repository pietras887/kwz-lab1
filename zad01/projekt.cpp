#include "projekt.h"

Projekt::Projekt(std::string nazwa_pliku)
{
	wczytaj(nazwa_pliku);
}

Projekt::~Projekt()
{
	delete[] czas_trwania;
	delete[] daneDzialania;
	for (int i = 0; i < lw; i++)
		delete[] macierz[i];
	delete[] macierz;
}

void Projekt::inicjuj()
{
	czas_trwania = new int[lw];
	macierz = new int*[lw];
	daneDzialania = new dzialanie[lw];
	czas = 0;
	for (int i = 0; i < lw; i++)
	{
		macierz[i] = new int[lw];
		daneDzialania[i].lateStart = std::numeric_limits<int>::max();
		daneDzialania[i].lateFinish = std::numeric_limits<int>::max();
		for (int j = 0; j < lw; j++)
			macierz[i][j] = 0;
	}
}

bool Projekt::wczytaj(std::string nazwa_pliku)
{
	std::ifstream plik(nazwa_pliku);
	if (!plik.is_open())
	{
		std::cerr << "Jest kiepsko :(" << std::endl;
		return false;
	}
	else{
		plik >> lw >> lk;
		inicjuj();
		polaczenia tmp;
		for (int i = 0; i < lw; i++){
			plik >> czas_trwania[i];
			tmp.nr = i;
			Polaczenia.push_back(tmp);

		}
		for (int i = 0; i < lk; i++)
		{
			int from, to;
			plik >> from >> to;
			macierz[from - 1][to - 1] = 1;
		}
		plik.close();
		return true;
	}
}

std::vector<int> Projekt::wyznaczPoprzedniki(const int i)
{
	std::vector<int> poprzedniki;
	for (int j = 0; j < lw; j++)
		if (macierz[j][i] == 1)
			poprzedniki.push_back(j);

	return poprzedniki;
}

std::vector<int> Projekt::czynnosciKoncowe()
{
	std::vector<int> czynnosci;

	for (int j = 0; j < lw; j++)
	{
		int iloscWyjsc = 0;
		for (int i = 0; i < lw; i++)
			if (macierz[j][i] == 1)
				iloscWyjsc++;

		if (iloscWyjsc == 0)
			czynnosci.push_back(j);
	}

	return czynnosci;
}

int Projekt::czasProjektuFB()
{
	int v = 0;
	int y_max = 0;
	std::vector<int> d(czas_trwania, czas_trwania+lw);
	std::vector<int> p(lw, -1);
	bool test = false;
	for (int i = 2; i <= lw && !test; i++)
	{
		test = true;
		for (int x = 0; x < lw; x++)
			for (int y = 0; y < lw; y++)
				if (macierz[x][y] == 1 && d[y] < d[x] + czas_trwania[y])
				{
					test = false;
					d[y] = d[x] + czas_trwania[y];
					p[y] = x;
					czas_projektu = std::max(d[y], czas_projektu);
					if (czas_projektu == d[y])
						y_max = y;
				}
	}
	for (int i = 0; i < lw; i++)
	{
		daneDzialania[i].earlyFinish = d[i];
		daneDzialania[i].earlyStart = d[i] - czas_trwania[i];
	}
	auto dzialania_konc = czynnosciKoncowe();
	std::vector<int>::iterator i = dzialania_konc.begin();
	for (; i != dzialania_konc.end() ;i++)
	{
		daneDzialania[*i].lateFinish = czas_projektu;
		daneDzialania[*i].lateStart = czas_projektu - czas_trwania[*i];
		wyznaczParametryDzialaniaLate(*i);
	}
	wyznaczSciezkaKrytyczna(y_max, p);
	return czas_projektu; 
}

void Projekt::wyznaczSciezkaKrytyczna(const int koniec, std::vector<int> tabPoprzednikow)
{
	int index = koniec;
	sciezkaKrytyczna = std::vector<int>();

	while (tabPoprzednikow[index] != -1)
	{
		sciezkaKrytyczna.push_back(index + 1);
		index = tabPoprzednikow[index];
	}

	sciezkaKrytyczna.push_back(index + 1);
	std::reverse(sciezkaKrytyczna.begin(), sciezkaKrytyczna.end());
}

std::vector<int> Projekt::getSciezkaKrytyczna()
{
	return sciezkaKrytyczna;
}

void Projekt::wyznaczParametryDzialaniaLate(const int i)
{
	auto poprzedniki = wyznaczPoprzedniki(i);
	std::vector<int>::iterator j = poprzedniki.begin();
	if (!poprzedniki.empty())
		for (; j != poprzedniki.end();j++)
		{
			daneDzialania[*j].lateFinish = std::min(daneDzialania[*j].lateFinish, daneDzialania[i].lateStart);
			daneDzialania[*j].lateStart = daneDzialania[*j].lateFinish - czas_trwania[*j];
			wyznaczParametryDzialaniaLate(*j);
		}
}

void Projekt::getParametryDzialania()
{
	std::cout << "ES\tEF\tLS\tLF" << std::endl;
	for (int i = 0; i < lw; i++)
	{
		std::cout << daneDzialania[i].earlyStart << "\t" << daneDzialania[i].earlyFinish << "\t";
		std::cout << daneDzialania[i].lateStart << "\t" << daneDzialania[i].lateFinish << std::endl;
	}
	
}
void Projekt::Sortowanie()
{
	std::vector<polaczenia> tmp; 

	for (int i = 0; i < lw; i++)
		for (int j = 0; j < lw; j++)
			if (macierz[i][j] == 1)
				Polaczenia[j].wchodzace++;
			else if (macierz[j][i] == 1)
				Polaczenia[i].wychodzace++;

	tmp = Polaczenia;

	while (!tmp.empty())
	{
		std::vector<polaczenia>::iterator iter;
		for (std::vector<polaczenia>::iterator it = tmp.begin(); it != tmp.end(); it++)
			if (!(Polaczenia[it->nr].wchodzace)) 
			{
				iter = it;
				break;
			}
	
		for (int i = 0; i < lw; i++)
			if (macierz[iter->nr][i])
				Polaczenia[i].wchodzace--;
	
		posortowaneWierzcholki.push_back(iter->nr);
		tmp.erase(iter);
	}
}


void Projekt::Relaksacja()
{
	int y_max = 0;
	std::vector<int> p(lw, -1);
	czas_projektu = 0;
	for (int i = 0; i < lw; i++)
	{
		std::vector<int> iloscWchodzacych;
		for (int j = 0; j < lw; j++)
			if (macierz[j][posortowaneWierzcholki[i]])
				iloscWchodzacych.push_back(j);

		int max = 0;
		for (unsigned int k = 0; k < iloscWchodzacych.size(); k++)
		if (max < daneDzialania[iloscWchodzacych[k]].earlyFinish)
			max = daneDzialania[iloscWchodzacych[k]].earlyFinish;


		daneDzialania[posortowaneWierzcholki[i]].earlyStart = max;
		daneDzialania[posortowaneWierzcholki[i]].earlyFinish = max + czas_trwania[posortowaneWierzcholki[i]];
		czas_projektu = std::max(czas_projektu, daneDzialania[posortowaneWierzcholki[i]].earlyFinish);
	}

	for (unsigned int i = posortowaneWierzcholki.size()-1; i > 0; i--)
	{
		std::vector<int> iloscWychodzacych;
		for (int j = 0; j < lw; j++)
			if (macierz[posortowaneWierzcholki[i]][j])
				iloscWychodzacych.push_back(j);

		int min = czas_projektu;
		if (!iloscWychodzacych.size())
		{
			daneDzialania[posortowaneWierzcholki[i]].lateFinish = czas_projektu;
			daneDzialania[posortowaneWierzcholki[i]].lateStart = czas_projektu - czas_trwania[posortowaneWierzcholki[i]];
		}
		else
		{
			for (unsigned int k = 0; k < iloscWychodzacych.size(); k++)
				if (min > daneDzialania[iloscWychodzacych[k]].lateStart)
					min = daneDzialania[iloscWychodzacych[k]].lateStart;

			daneDzialania[posortowaneWierzcholki[i]].lateFinish = min;
			daneDzialania[posortowaneWierzcholki[i]].lateStart = min - czas_trwania[posortowaneWierzcholki[i]];
		}
	}
	sciezkaKrytyczna.clear();
	int tmp = czas_projektu;
	while (tmp)
		for (unsigned int i = 0; i < posortowaneWierzcholki.size(); i++)
			if (daneDzialania[posortowaneWierzcholki[i]].earlyFinish == tmp)
			{
				sciezkaKrytyczna.push_back(posortowaneWierzcholki[i]+1);
				tmp = tmp - czas_trwania[posortowaneWierzcholki[i]];
				break;
			}

	std::cout << "Sciezka krytyczna" << std::endl;
	for (unsigned int i = sciezkaKrytyczna.size(); i > 0; i--)
		std::cout << sciezkaKrytyczna[i-1] << std::endl;

}












